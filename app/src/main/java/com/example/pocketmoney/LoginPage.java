package com.example.pocketmoney;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pocketmoney.database.DatabaseHelper;
import com.example.pocketmoney.database.UserTable;

import java.util.ArrayList;
import java.util.List;

public class LoginPage extends AppCompatActivity {

    private EditText ET_loginName,ET_password;
    private Button BT_login;
    private TextView TV_register;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        ET_loginName = findViewById(R.id.ET_loginName);
        ET_password = findViewById(R.id.ET_RegPass);
        BT_login = findViewById(R.id.BT_login);
        TV_register = findViewById(R.id.TV_register);

        databaseHelper = new DatabaseHelper(this);

        BT_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String loginName = ET_loginName.getText().toString();
                String loginPassword = ET_password.getText().toString();

                loginValidation(loginName,loginPassword);


            }
        });

        TV_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginPage.this,Registration_page.class);
                startActivity(intent);
            }
        });
    }


    private void loginValidation(String ETname,String ETpassword){

        Cursor cursor = databaseHelper.dataRetrieve();
        List <UserTable> loginDataInfo = new ArrayList<UserTable>();

        while (cursor.moveToNext()){

           String name = cursor.getString(1);
           String password = cursor.getString(3);

           if(name.equals(ETname) && password.equals(ETpassword)){

               Intent intent = new Intent(LoginPage.this,ExpensesActivity.class);
               Toast.makeText(LoginPage.this, "login is successful", Toast.LENGTH_SHORT).show();
               startActivity(intent);
               break;

           } else{
               Toast.makeText(this, "You have no account", Toast.LENGTH_SHORT).show();

           }





        }

    }
}
