package com.example.pocketmoney.database;


public class UserTable {

    private String name;
    private String email;
    private String password;
    private  String number;
    private String gender;

    public UserTable(String name, String email, String password, String number, String gender) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.number = number;
        this.gender = gender;
    }


    public UserTable(String name, String password) {
        this.name = name;
        this.password = password;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
