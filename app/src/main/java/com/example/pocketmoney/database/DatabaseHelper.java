package com.example.pocketmoney.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;


public class DatabaseHelper extends SQLiteOpenHelper {

    private  static String DATABASE_NAME = "pocketMoney.db";
    private  static int VERSION = 1;

    private String TABLE_NAME = "user";
    private String ID = "id";
    private String NAME = "name";
    private String EMAIL = "email";
    private String PASSWORD = "password";
    private String NUMBER = "number";
    private String GENDER = "gender";

    public DatabaseHelper(Context context) {
        super(context,DATABASE_NAME,null,VERSION);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE "+TABLE_NAME+" ("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT  ,"+NAME+ " TEXT ,"+EMAIL+" TEXT ,"
                +PASSWORD+" TEXT ,"+NUMBER+" TEXT ,"+GENDER+" TEXT "+" )");



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);


    }

    // For insert Data to Database . . . . . . . ..  . . . . .

    public boolean insertDataMethod(String regName,String regEmail ,String regPassword,String regNumber,String regGender){

        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("name",regName);
        values.put("email",regEmail);
        values.put("password",regPassword);
        values.put("number",regNumber);
        values.put("gender",regGender);

      long check =  database.insert(TABLE_NAME,null,values);

        Log.d("check", String.valueOf(check));
        if(check == -1){
            return false;
        }
        else {

            return true;
      }
    }

    // Retrieve Data from Database . . . . . . . . . . . . . . .

    public Cursor dataRetrieve(){

       SQLiteDatabase database = this.getReadableDatabase();

       Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_NAME, null);

       return cursor;

    }
}
