package com.example.pocketmoney;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pocketmoney.database.DatabaseHelper;

public class Registration_page extends AppCompatActivity {

    private EditText ET_RegName,ET_RegEmail,ET_RegPassword,ET_RegNumber,ET_RegGender;
    private Button BT_signUp;

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_page);

        ET_RegName = findViewById(R.id.ET_RegName);
        ET_RegEmail = findViewById(R.id.ET_RegEmail);
        ET_RegPassword = findViewById(R.id.ET_RegPassword);
        ET_RegNumber = findViewById(R.id.ET_RegNumber);
        ET_RegGender = findViewById(R.id.ET_RegGender);

        BT_signUp = findViewById(R.id.BT_signUp);

        databaseHelper = new DatabaseHelper(this);

        BT_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String regName = ET_RegName.getText().toString();
                String regEmail = ET_RegEmail.getText().toString();
                String regPassword = ET_RegPassword.getText().toString();
                String regNumber = ET_RegNumber.getText().toString();
                String regGender = ET_RegGender.getText().toString();

                boolean checkSignUP = databaseHelper.insertDataMethod(regName,regEmail,regPassword,regNumber,regGender);

                if (checkSignUP){

                    Intent intent = new Intent(Registration_page.this,LoginPage.class);
                    startActivity(intent);
                    Toast.makeText(Registration_page.this, "Sign up has been Successful", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(Registration_page.this, "please check your input", Toast.LENGTH_SHORT).show();
                }

                regName = "";
                regEmail = "";
                regPassword = "";
                regNumber = "";
                regGender = "";

            }
        });

    }
}
